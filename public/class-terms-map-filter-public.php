<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://webstruxure.co.nz
 * @since      1.0.0
 *
 * @package    Terms_Map_Filter
 * @subpackage Terms_Map_Filter/public
 */

/**
 * Set up dependencies.
 */
require plugin_dir_path( __DIR__ ) . 'vendor/autoload.php';

use League\Csv\Reader;
use League\Csv\Statement;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, hooks to enqueue the public-facing styles
 * and scripts, and a shortcode.
 *
 * @package    Terms_Map_Filter
 * @subpackage Terms_Map_Filter/public
 * @author     Webstruxure <daniel@webstruxure.co.nz>
 */
class Terms_Map_Filter_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since  1.0.0
	 * @access private
	 * @var    string  $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since  1.0.0
	 * @access private
	 * @var    string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the CSS for the plugin.
	 *
	 * @since 1.0.0
	 */
	public function terms_map_style() {

		wp_register_style(
			'terms-map',
			plugin_dir_url( __FILE__ ) . $this->get_asset_path( 'bundle.css' ),
			array(),
			$this->version,
			'all'
		);

	}

	/**
	 * Register the JS for the plugin.
	 *
	 * @since 1.0.0
	 * @todo Update theme to enqueue main stylesheet correctly to allow inlining plugin styles in head.
	 */
	public function terms_map_script() {

		wp_register_script(
			'vue',
			'https://unpkg.com/vue@2.5.16/dist/vue.min.js',
			array(),
			'2.5.16',
			'all'
		);

		wp_register_script(
			'terms-map-js',
			plugin_dir_url( __FILE__ ) . $this->get_asset_path( 'bundle.js' ),
			array( 'vue' ),
			$this->version,
			'all'
		);

		wp_localize_script(
			'terms-map-js',
			'ss',
			array( 'terms' => $this->prepare_csv_data() )
		);

	}

	/**
	 * Set up a shortcode to render the plugin output.
	 *
	 * Conditionally loads plugin assets when shortcode is active on page.
	 *
	 * @since 1.0.0
	 */
	public function terms_map_shortcode() {

		/**
		 * Sets up the shortcode.
		 *
		 * Plugin assets are conditionally loaded when shortcode is present.
		 */
		wp_enqueue_style( 'terms-map' );
		wp_enqueue_script( 'vue' );
		wp_enqueue_script( 'terms-map-js' );

		include plugin_dir_path( __FILE__ ) . 'partials/terms-map-filter-gui.php';

	}

	/**
	 * Use assets with hashed names.
	 *
	 * Matches a filename against a hash manifest and returns the hash file name if
	 * it exists.
	 *
	 * @param string $filename Original name of the file.
	 * @return string Hashed name version of a file.
	 */
	private function get_asset_path( $filename ) {
		$manifest_path = plugin_dir_path( __FILE__ ) . 'dist/manifest.json';

		if ( file_exists( $manifest_path ) ) {
			$manifest = json_decode( file_get_contents( $manifest_path ), true );

			if ( array_key_exists( $filename, $manifest ) ) {
				return 'dist/' . $manifest[ $filename ];
			}
		}
	}

	/**
	 * Read data from the CSV.
	 *
	 * @since 1.0.0
	 * @return array Prepare terms for conversion to JSON.
	 */
	private function prepare_csv_data() {
		if ( ! ( file_exists( plugin_dir_path( __DIR__ ) . '/public/data/map-of-the-funding-terms.csv' ) ) ) {
			$terms = [];
			return $terms;

		} else {
			$csv = Reader::createFromPath( plugin_dir_path( __DIR__ ) . '/public/data/map-of-the-funding-terms.csv', 'r' );
			$csv->setHeaderOffset( 0 );
			$total_records = count( $csv );

			$records = $csv->getRecords( [ 'name', 'friendsAndFamily', 'seed', 'seriesA', 'seriesB', 'seriesC', 'description' ] );

			/**
			 * Set up loop.
			 */
			$loop_offset       = 1; // A starting value for the first loop.
			$loop_offset_group = 3; // The row count (inclusive) from a term to the next term.

			while ( $loop_offset_group <= $total_records + 1 ) : // Account for each record containing two rows of data.

				$col_friends_and_family = [];
				$col_seed               = [];

				/**
				 * Loop count increases by 2 each time.
				 */
				foreach ( $records as $offset => $row ) {

					if ( $loop_offset === $offset ) {
						$col_name        = $row['name'];
						$col_series_a    = $row['seriesA'];
						$col_series_b    = $row['seriesB'];
						$col_series_c    = $row['seriesC'];
						$col_description = $row['description'];
					}

					if ( $offset >= $loop_offset && $offset < $loop_offset_group ) {
						$col_friends_and_family[] = $row['friendsAndFamily'];
						$col_seed[]               = $row['seed'];
					}
				}

				$loop_offset       = $loop_offset_group;
				$loop_offset_group = $loop_offset_group + 2;

				$term['name']             = $col_name;
				$term['friendsAndFamily'] = $col_friends_and_family;
				$term['seed']             = $col_seed;
				$term['seriesA']          = $col_series_a;
				$term['seriesB']          = $col_series_b;
				$term['seriesC']          = $col_series_c;
				$term['description']      = $col_description;

				$terms[] = $term;

			endwhile;

			return $terms;
		}
	}
}
