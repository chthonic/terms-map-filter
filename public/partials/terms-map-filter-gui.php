<?php
/**
 * The terms map filter markup
 *
 * Renders a UI for filtering terms.
 *
 * @link       https://webstruxure.co.nz
 * @since      1.0.0
 *
 * @package    Terms_Map_Filter
 * @subpackage Terms_Map_Filter/public/partials
 */

?>

<div id="terms-map" class="terms-map" v-cloak>
	<template v-if="terms.length === 0">
		<p>Apologies, our map of funding terms is currently unavailable.</p>
	</template>

	<template v-else>
		<template v-if="showTerms">
			<button type="button"
			class="toggle"
			:class="{'toggle--is-open': showTerms}"
			@click="getLegend()">
				<transition>
				<span class="icon icon--toggle"><span class="▾"></span></span>
				</transition>
				<span class="label">funding terms</span>
			</button>
		</template>

		<template v-else>
			<button type="button"
			class="toggle"
			:class="{'toggle--is-open': showTerms}"
			@click="showTerms = !showTerms">
				<transition>
				<span class="icon icon--toggle"><span class="▾"></span></span>
				</transition>
				<span class="label">funding terms</span>
			</button>
		</template>

		<transition name="fade" mode="out-in">
			<div v-if="showTerms" key="terms" class="terms-map__all">
				<ul>
					<li class="term"
					v-for="(term, key) of terms"
					:key="key">
						<div>
							<a class="term__name" href="#" @click.prevent="getTerm(key)">
								{{ term.name }}
							</a>

							<tool-tip :term="term"></tool-tip>
						</div>
					</li>
				</ul>
			</div>

			<div v-else key="term" class="terms-map__single">
				<h1>term: {{ term.name }}</h1>
				<p v-if="term.description">{{ term.description }}</p>

				<dl :class="['definition', 'definition--' + setTermAsClassname(term.name)]">
					<div>
						<dt>family and friends</dt>
						<template v-for="(item, key) of term.friendsAndFamily"
						v-if="item">
							<dd :class="key === 1 ? 'convertible' : 'standard'" 
							v-html="item"
							v-alignment="key"></dd>
						</template>
					</div>

					<div>
						<dt>seed</dt>
						<template v-for="(item, key) of term.seed"
						v-if="item">
							<dd :class="key === 1 ? 'convertible' : 'standard'"
							v-html="item"></dd>
						</template>
					</div>

					<div>
						<dt>series A</dt>
						<dd v-html="term.seriesA"></dd>
					</div>

					<div>
						<dt>series B</dt>
						<dd v-html="term.seriesB"></dd>
					</div>

					<div>
						<dt>series C+</dt>
						<dd v-html="term.seriesC"></dd>
					</div>
				</dl>

				<span class="legend">* convertible notes</span>
			</div>
		</transition>
	</template>
</div>
