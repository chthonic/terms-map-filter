const tip = Vue.component('tool-tip', {
	template: `
	<span class="term__tip" ref="tip">
		<button type="button" @click="getTip()">?</button>
		<span :class="['tip', {'tip--is-open': showTip}, {'tip--flip': xPos < 320}]">
		{{ term.description }}
		</span>
	</span>
	`,
	data () {
		return {
			showTip: false,
			xPos: Number
		}
	},
	props: ['term'],
	methods: {
		getTip () {
			// Find the x position of the current tip container.
			let tip = this.$refs.tip
			this.xPos = tip.getBoundingClientRect().left

			this.showTip = ! this.showTip
		},
		removeTip(e) {
			if (! this.$el.contains(e.target) ) {
			  this.showTip = false
			}
		}
	},
	created() {
		window.addEventListener('click', this.removeTip)
	},
	beforeDestroy() {
		window.removeEventListener('click', this.removeTip)
	}  
});

/**
 * Bootstrap the terms map filter.
 */
const termsMap = new Vue({
	el: '#terms-map',
	data () {
		return {
			showTerms: true,
			term: '',
			terms: ss.terms
		}			
	},
	methods: {
		getTerm (key) {
			this.showTerms = false
			this.term = this.terms[key]
		},
		getLegend () {
			this.showTerms = false
			this.term = this.terms[0]
		},
		setTermAsClassname (term) {
			term = term.replace(/\s+/g, '-')
			return term
		}
	},
	directives: {
		alignment: {
			inserted: function(el, binding){
				if (binding.value === 0) { // Filter results by key to target the first `<dd>`.
					let cells = document.querySelectorAll('.standard')
					let heights = []

					// Store each cell height.
					for (let height of cells) {
						heights.push(height.offsetHeight)
					}

					// Find the largest cell height.
					let setHeight = Math.max(...heights)
					
					// Set all cells to match the largest cell height.
					for (var i = 0; i < cells.length; i++) {
						cells[i].style.height = (setHeight - 11) + 'px' // Magic "11" accounts for white-space preservation!
					}
				}
			}
		}
	},
	mounted () {
		this.term = this.terms[0]
	}
})
