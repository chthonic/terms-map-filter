/**
 * @todo chunking by template; fallback bundle.js and style.css in theme root;
 * Note: gulp-sass installed to install node-sass; was unable to install directly.
 */
const path = require('path');
const autoprefixer = require('autoprefixer');
const fs = require('fs');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        bundle: './public/src/index'
    },
    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: 'js/[name].[chunkhash].min.js'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].[chunkhash].min.css'
        }),
        new WebpackAssetsManifest(),
        new CleanWebpackPlugin(['public/dist'])
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/(node_modules)/],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: () => [
                                autoprefixer({
                                    browsers: [
                                        '>1%',
                                        'last 4 versions',
                                        'not ie < 9'
                                    ]
                                })
                            ]
                        }
                    },
                    'sass-loader'
                ]
            }
        ]
    }
}