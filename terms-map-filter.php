<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://webstruxure.co.nz
 * @since             1.0.0
 * @package           Terms_Map_Filter
 *
 * @wordpress-plugin
 * Plugin Name:       Webx Terms Map Filter
 * Plugin URI:        https://bitbucket.org/chthonic/terms-map-filter/src/master/
 * Description:       Generates a UI to filter terms from a CSV file.
 * Version:           1.2.0
 * Author:            Webstruxure
 * Author URI:        https://webstruxure.co.nz
 * Developer:         Daniel Shaw
 * Developer URI:     https://danielshaw.co.nz
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       terms-map-filter
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Current plugin version.
 */
define( 'PLUGIN_NAME_VERSION', '1.2.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-terms-map-filter-activator.php
 */
function activate_terms_map_filter() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-terms-map-filter-activator.php';
	Terms_Map_Filter_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-terms-map-filter-deactivator.php
 */
function deactivate_terms_map_filter() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-terms-map-filter-deactivator.php';
	Terms_Map_Filter_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_terms_map_filter' );
register_deactivation_hook( __FILE__, 'deactivate_terms_map_filter' );

/**
 * The core plugin class that is used to define public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-terms-map-filter.php';

/**
 * Begins execution of the plugin.
 *
 * Everything within the plugin is registered via hooks,
 * so kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 */
function run_terms_map_filter() {
	$plugin = new Terms_Map_Filter();
	$plugin->run();
}

run_terms_map_filter();
