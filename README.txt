=== Plugin Name ===
Contributors: @chthon
Tags: filter
Requires at least: 4.9.5
Tested up to: 4.9.5
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Generates a terms map filter from a CSV file.

== Installation ==

1. Upload the `webx-terms-map` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Use the `[terms-map]` shortcode in pages and posts.

== Changelog ==

= 1.2.0
* Change layout model from CSS Columns to Flexbox.

= 1.1.0 =
* Added term tips.

= 1.0 =
* Initial version.
