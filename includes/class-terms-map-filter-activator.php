<?php
/**
 * Fired during plugin activation.
 *
 * @link       https://webstruxure.co.nz
 * @since      1.0.0
 *
 * @package    Terms_Map_Filter
 * @subpackage Terms_Map_Filter/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Terms_Map_Filter
 * @subpackage Terms_Map_Filter/includes
 * @author     Webstruxure <daniel@webstruxure.co.nz>
 */
class Terms_Map_Filter_Activator {

	/**
	 * Short Description.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
